import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { ObservbasicComponent } from './components/observbasic/observbasic.component';
import { ObservdemoComponent } from './components/observdemo/observdemo.component';
import { NoobservService } from './services/noobserv.service';
import { SimpleserviceComponent } from './components/simpleservice/simpleservice.component';

const routes: Routes = [
  {path: 'basic', component: ObservbasicComponent},
  {path: 'simple', component: SimpleserviceComponent},
  {path: 'demo', component: ObservdemoComponent},
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule, RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})

export class AppRoutingModule { }
