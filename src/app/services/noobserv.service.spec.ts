import { TestBed } from '@angular/core/testing';

import { NoobservService } from './noobserv.service';

describe('NoobservService', () => {
  let service: NoobservService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NoobservService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
