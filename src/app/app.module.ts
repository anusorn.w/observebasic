import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ObservbasicComponent } from './components/observbasic/observbasic.component';
import { ObservdemoComponent } from './components/observdemo/observdemo.component';

import { NoobservService } from './services/noobserv.service';
import { ObservService } from './services/observ.service';
import { SimpleserviceComponent } from './components/simpleservice/simpleservice.component';
import { ComponentAComponent } from './components/observdemo/component-a/component-a.component';
import { ComponentBComponent } from './components/observdemo/component-b/component-b.component';

@NgModule({
  declarations: [
    AppComponent,
    ObservbasicComponent,
    ObservdemoComponent,
    SimpleserviceComponent,
    ComponentAComponent,
    ComponentBComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
  ],
  providers: [NoobservService,ObservService],
  bootstrap: [AppComponent]
})
export class AppModule { }
