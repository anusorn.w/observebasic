import { Component, OnInit } from '@angular/core';
import { NoobservService } from 'src/app/services/noobserv.service';

@Component({
  selector: 'app-simpleservice',
  templateUrl: './simpleservice.component.html',
  styleUrls: ['./simpleservice.component.css']
})
export class SimpleserviceComponent implements OnInit {
  newKid : string='';
  Kids : string[] = [];

  constructor(private dservice:NoobservService) { }

  ngOnInit(): void {
    this.getKids();
   }

  addData() {
    if (this.newKid === '') {
      return;
    }
    this.dservice.addNewKid(this.newKid);
    this.newKid='';
  }
  getKids() {
    this.Kids = this.dservice.getAll();
  }

}
