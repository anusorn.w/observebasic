import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ObservdemoComponent } from './observdemo.component';

describe('ObservdemoComponent', () => {
  let component: ObservdemoComponent;
  let fixture: ComponentFixture<ObservdemoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ObservdemoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ObservdemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
