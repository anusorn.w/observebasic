import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NoobservService {
  csZoo:string[]= [];

  constructor() { }

  addNewKid(nKid:string):void {
    this.csZoo.push(nKid);
    console.log(this.csZoo);
  }

  getAll():string[] {
    return this.csZoo;
  }
}
