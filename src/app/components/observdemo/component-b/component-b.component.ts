import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ObservService } from 'src/app/services/observ.service';

@Component({
  selector: 'app-component-b',
  templateUrl: './component-b.component.html',
  styleUrls: ['./component-b.component.css']
})
export class ComponentBComponent implements OnInit {
  blist:string[] = [];
  obs!:Subscription;

  constructor(private dservice:ObservService) { }

  ngOnInit(): void {
    this.obs = this.dservice.list$.subscribe(list => (this.blist = list));
  }

  remove(l:string) {
    this.dservice.removeList(l);
  }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.obs.unsubscribe();
  }

}
