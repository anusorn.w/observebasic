/*https://www.geeksforgeeks.org/angular-7-observables/*/

import { Component, OnInit } from '@angular/core';
import { Observable, Observer, Subscription, of } from 'rxjs'

@Component({
  selector: 'app-observbasic',
  templateUrl: './observbasic.component.html',
  styleUrls: ['./observbasic.component.css']
})
export class ObservbasicComponent implements OnInit {

  constructor() { }

  title = 'observ';
  /*
    obs = new Observable((observer) =>   {
      console.log("Observable starts...");
      observer.next("1");
      observer.next("2");
      observer.next("3");
      observer.next("4");
      observer.next("5");
    });


    ngOnInit() {
      const sub = this.obs.subscribe(val=> console.log(val));
    }

    ngOnInit() {
      const sqnc = new Observable(count1to10);

      sqnc.subscribe(num => console.log(num));

      function count1to10(observer:any) {
        for (let i=1;i<=5;i++) {
          observer.next(i);
        }
        return {unsubscribe(){}};
      }
    }
    */
   //observable variable
  obsable1$ !: Observable<string>;
  obsable2$ !: Observable<void>;
  obsable3$ !: Observable<{isDiby3: boolean, num: number}>;
  subscribe3TF !: boolean;
  subscribe3N !: number;

  //observer
  observer1 !: Observer<string>;
  //observer2 !: Observer<void>;
  //subscription
  subscribe1 !:Subscription;
  subscribe2 !:Subscription;
  subscribe3 !:Subscription;

  ngOnInit(): void {
    //ex1
    this.obsable1$ = of('abc','ddd','eee','zzz');
    this.observer1 = {next:(x:string) => console.log('Observer1 got a next value:'+ x),
      error: (error: string) => console.log('observer1 got an error:' + error),
      complete: () => console.log('observer1 got a complete notificationtion'),
    };
    this.subscribe1 = this.obsable1$.subscribe(this.observer1);

    //ex2 https://www.koderhq.com/tutorial/angular/observable/
    this.obsable2$ = new Observable(count1to10);
    function count1to10(observer2:any) {
      for (let i=1;i<=5;i++) {
        observer2.next(i);
      }
      observer2.complete();
      return {unsubscribe(){}};
    }
    this.subscribe2 = this.obsable2$.subscribe({
      next(msg) { console.log(msg); },
      complete() { console.log('observer2 got a complete notificationtion'); }
    });

    //ex3 https://zeroesandones.medium.com/how-to-work-with-observables-in-angular-9-99884ceab56
    // https://www.netjstech.com/2020/09/how-to-create-custom-observable-angular.html
    //let’s instantiate a class level observable object that returns an object which contains two properties:
    // isDivisibleByThree — Boolean value indicating whether the current number is divisible by three.
    //number — The number that will be checked for mod 3.
    this.obsable3$ = new Observable((observer3)=> {
      let n = 0;

      setInterval(() => {
        if (n % 3 === 0) {
          observer3.next({ isDiby3:true, num: n });
        } else {
          observer3.next({ isDiby3:false, num: n });
        }
        n++;
      },1000);
    });
  }
  sub3() {
    this.subscribe3 = this.obsable3$.subscribe({
      next:(value) => {
        console.log(`subscriber3-Number: ${value.num} ${value.isDiby3 ? 'is':'is not'} divisible by 3.`);
        this.subscribe3TF=value.isDiby3;
        this.subscribe3N=value.num;
      }
    });
  }

  unsub3() {
    this.subscribe3.unsubscribe();
    console.log('suscriber3 unsubscribed from the observerble');
  }
}
