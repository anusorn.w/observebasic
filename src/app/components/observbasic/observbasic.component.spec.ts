import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ObservbasicComponent } from './observbasic.component';

describe('ObservbasicComponent', () => {
  let component: ObservbasicComponent;
  let fixture: ComponentFixture<ObservbasicComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ObservbasicComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ObservbasicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
