import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ObservService {
  private csZoo: string[] = [];
  private list = new BehaviorSubject<string[]>([]);
  readonly list$ = this.list.asObservable();

  constructor() {
    console.log("start");
   }

   ngOnInit() {
   }

  addNewList(l:string) {
    console.log(l);
    this.csZoo.push(l);
    this.list.next(this.csZoo);
    console.log(`after add '${l}' : ${JSON.stringify(this.csZoo)}`);
  }

  getAll(){
    return this.csZoo;
  }
  removeList(l:string) {
    this.csZoo = this.csZoo.filter(v => v!== l);
    this.list.next(this.csZoo);
    console.log(`after remove '${l}' : ${JSON.stringify(this.csZoo)}`);
  }
}
