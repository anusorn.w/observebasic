import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SimpleserviceComponent } from './simpleservice.component';

describe('SimpleserviceComponent', () => {
  let component: SimpleserviceComponent;
  let fixture: ComponentFixture<SimpleserviceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SimpleserviceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleserviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
