import { Component, OnInit } from '@angular/core';
import { ObservService } from 'src/app/services/observ.service';

@Component({
  selector: 'app-component-a',
  templateUrl: './component-a.component.html',
  styleUrls: ['./component-a.component.css']
})

export class ComponentAComponent implements OnInit {
  allKid:string[]=[];
  kid:string='';

  constructor(private dservice:ObservService) { }

  ngOnInit(): void {
    //this.getAllKid();
  }

  addNewKid() {
    if (this.kid === '') {
      return;
    }
    this.dservice.addNewList(this.kid);
    this.kid='';
  }

  getAllKid() {
    //return this.allKid = this.dservice.getAll();
  }

}
